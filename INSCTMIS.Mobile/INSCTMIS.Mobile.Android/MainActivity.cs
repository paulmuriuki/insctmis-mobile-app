﻿using Android.App;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Widget;
using INSCTMIS.Mobile.Helpers;
using Plugin.CurrentActivity;
using Plugin.Media;
using Plugin.Permissions;
using Android.OS;
using FormsToolkit.Droid;
using Xamarin.Forms;
using Xamarin.Forms.DataGrid;
using Xamarin.Forms.Platform.Android;

namespace INSCTMIS.Mobile.Droid
{
    [Activity(
        Label = "IN-SCTMIS Mobile Data Tool (Ethiopia)", 
        Icon = "@drawable/logo", 
        Theme = "@style/MainTheme", 
        MainLauncher = true, 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation

                               )]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            await CrossMedia.Current.Initialize();
           Forms.SetFlags("FastRenderers_Experimental");
            Forms.Init(this, savedInstanceState);
            DataGridComponent.Init();
            Toolkit.Init();
            CrossCurrentActivity.Current.Init(this, savedInstanceState); 
            CrossCurrentActivity.Current.Activity = this;
            var gpsAvailable = IsPlayServicesAvailable();
            Settings.Current.PushNotificationsEnabled = gpsAvailable;
            LoadApplication(new App());
        }
        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    if (Settings.Current.GooglePlayChecked)
                        return false;

                    Settings.Current.GooglePlayChecked = true;
                    Toast.MakeText(this, "Google Play services is not installed, push notifications have been disabled.", ToastLength.Long).Show();
                }
                else
                {
                    Settings.Current.PushNotificationsEnabled = false;
                }
                return false;
            }
            else
            {
                Settings.Current.PushNotificationsEnabled = true;
                return true;
            }
        }   
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
             PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}