﻿using INSCTMIS.Mobile.Models;
using INSCTMIS.Mobile.Pages;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        private MainPage RootPage { get => Application.Current.MainPage as MainPage; }

        private List<HomeMenuItem> menuItems;

        public MenuPage()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.HomePage, Title="Home" },
                new HomeMenuItem {Id = MenuItemType.HouseholdProfilePage, Title="Household Profile" },
                new HomeMenuItem {Id = MenuItemType.CompliancePage, Title="Compliance" },
                new HomeMenuItem {Id = MenuItemType.MonitoringPage, Title="Monitoring" },
                new HomeMenuItem {Id = MenuItemType.RetargetingPage, Title="Retargeting" },
                new HomeMenuItem {Id = MenuItemType.SyncPage, Title="Data Transfer" },
                new HomeMenuItem {Id = MenuItemType.LogoutPage, Title="Logout" },
            };

            ListViewMenu.ItemsSource = menuItems;
            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    return;
                }

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };
        }
    }
}