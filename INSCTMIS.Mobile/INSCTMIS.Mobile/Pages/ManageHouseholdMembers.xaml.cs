﻿using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ManageHouseholdMembers : ContentPage
    {
        public ManageHouseholdMembers()
        {
            InitializeComponent();
            BindingContext = new ManageHouseholdMemberViewModel(Navigation);
        }

        public ManageHouseholdMembers(int HouseholdId)
        {
            InitializeComponent();
            BindingContext = new ManageHouseholdMemberViewModel(Navigation, HouseholdId);
        }
    }
}