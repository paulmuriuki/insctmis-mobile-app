﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HouseholdProfilePage : ContentPage
	{
        private HouseholdProfileViewModel vm;

        public HouseholdProfilePage ()
		{
			InitializeComponent ();
            BindingContext = vm = new HouseholdProfileViewModel(Navigation);
        }
	}
}