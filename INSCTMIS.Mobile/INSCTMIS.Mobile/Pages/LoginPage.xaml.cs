﻿using System;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Helpers;
using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
	    private LoginViewModel vm;

	    public LoginPage()
	    {
	        InitializeComponent();

	        BindingContext = vm = new LoginViewModel(Navigation);
	    }

	    protected override bool OnBackButtonPressed()
	    {
	        return Settings.Current.FirstRun || base.OnBackButtonPressed();
	    }
	    private async void OnTermsClicked(object sender, EventArgs e)
	    {
	        Device.OpenUri(new Uri(Constants.BaseSiteAddress + "/public/terms-and-conditions/"));
	    }
    }
}