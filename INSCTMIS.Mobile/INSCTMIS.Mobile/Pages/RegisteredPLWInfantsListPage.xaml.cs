﻿using INSCTMIS.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisteredPLWInfantsListPage : ContentPage
    {
        public RegisteredPLWInfantsListPage(int PLWId)
        {
            InitializeComponent();
            BindingContext = new RegisteredPLWInfantsListViewModel(PLWId);
        }
    }
}