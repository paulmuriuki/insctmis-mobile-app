﻿using INSCTMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace INSCTMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ComplianceHouseholdsListPage : TabbedPage
	{
        private ComplianceHouseholdsListViewModel vm;

        public ComplianceHouseholdsListPage ()
		{
			InitializeComponent ();
            BindingContext = vm = new ComplianceHouseholdsListViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext is ComplianceHouseholdsListViewModel bindingContext)
            {
                bindingContext.OnAppearing();
                vm.DownloadedTDSComplianceHouseholds.ReplaceRange(bindingContext.GetTDSHouseholdsReadyForCompliance());
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            vm.DownloadedTDSComplianceHouseholds.Clear();

        }

    }
}