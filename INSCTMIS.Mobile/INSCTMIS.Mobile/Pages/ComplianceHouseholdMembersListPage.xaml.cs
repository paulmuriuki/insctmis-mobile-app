﻿using INSCTMIS.Mobile.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace INSCTMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ComplianceHouseholdMembersListPage : TabbedPage
    {

        public ComplianceHouseholdMembersListPage(Guid hhid)
        {
            InitializeComponent();
            BindingContext = new ComplianceHouseholdMembersListViewModel(Navigation, hhid);
        }
    }
}