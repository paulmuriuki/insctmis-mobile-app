﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Services;
using INSCTMIS.Mobile.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class ManagePLWViewModel : LocalBaseViewModel
    {
        public ManagePLWViewModel(INavigation navigation) : base(navigation)
        {
            db = App.Database;
            generalServices = new GeneralServices();
            LoadOptions();
            SaveNewPLWCommand = new Command(async () => await SavePLWCommand());

            _validator = new PLWValidator();
        }

        async Task SavePLWCommand()
        {
            IsBusy = true;
            Message = "Validating PLW data... ";

            var validationResult = _validator.Validate(this);

            if (!validationResult.IsValid)
            {
                ValidateMessage = GetErrorListFromValidationResult(validationResult);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "PLW Registration Error(s)",
                    Message = ValidateMessage,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }

            try
            {

                Form1B plw = new Form1B()
                {
                    RegionID = RegionID,
                    WoredaID = WoredaID,
                    KebeleID = KebeleID,
                    Gote = Gote,
                    Gare = Gare,
                    PLW = PLW,
                    NameOfPLW = NameOfPLW,
                    HouseHoldIDNumber = HouseHoldIDNumber,
                    CBHIMembership = SelectedMembershipOption,
                    CBHINumber = CBHINumber,
                    CollectionDate = CollectionDate,
                    SocialWorker = db.GetTableRows<SocialWorker>("SocialWorker")[0].UserId,
                    Remarks = Remarks,
                    CCCCBSPCMember = CCCCBSPCMember,
                    MedicalRecordNumber = MedicalRecordNumber,
                    PLWAge = PLWAge,
                    StartDateTDS = StartDateTDS,
                    EndDateTDS = EndDateTDS,
                    NutritionalStatusPLW = NutritionalStatusPLW,
                    ChildProtectionRisk = ChildProtectionRisk
                };

                db.Create(plw);
                MessagingCenter.Send(this, "Add PLW", plw);
                await Navigation.PopAsync(true);
                return;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to save new PLW.  ",
                    Message = ex.Message,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }
        }

        void LoadOptions()
        {
            RegionList = db.GetTableRows<Database.Region>("Region");
            PregantOrLactatingOptions = generalServices.GetPregnantOrLactating();
            NutritionalStatusOptions = generalServices.GetNutritionalStatus();
            YesNoOptions = generalServices.GetYesNo();
        }

        public Command SaveNewPLWCommand { get; set; }

        private readonly IValidator _validator;

        private DataStore db;
        private GeneralServices generalServices;
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string PLW { get; set; } //Pregnant/Lactating. Either/Or
        private GeneralOptions _selectedPLStatus { get; set; }
        public GeneralOptions SelectedPLStatus
        {
            get => _selectedPLStatus;
            set
            {
                if (_selectedPLStatus != value)
                {
                    _selectedPLStatus = value;
                    PLW = SelectedPLStatus.ID;
                    OnPropertyChanged(nameof(PLW));
                }
            }
        }
        public string NameOfPLW { get; set; } // Name of PLW incl. name of grandfather
        public string HouseHoldIDNumber { get; set; } // PSNP Number
        public string CCCCBSPCMember { get; set; } //CCC Member
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CollectionDate { get; set; }
        public string Remarks { get; set; }
        public string MedicalRecordNumber { get; set; } // Individual ID in Health Family Folder
        public string PLWAge { get; set; } //Age of PLW
        public string StartDateTDS { get; set; } //Transitioning from PW to temporary
        public string EndDateTDS { get; set; } //Expected temporary DS end Date
        public string NutritionalStatusPLW { get; set; }
        public string ChildProtectionRisk { get; set; }

        //Lists
        public List<Database.Region> RegionList { get; set; }
        private Database.Region _selectedRegion { get; set; }
        public Database.Region SelectedRegion
        {
            get { return _selectedRegion; }
            set
            {
                if (_selectedRegion != value)
                {
                    _selectedRegion = value;
                    RegionID = SelectedRegion.RegionID;
                    //update the Woreda list
                    WoredaList = App.Database
                                    .GetTableRows<Woreda>("Woreda")
                                    .FindAll(w => w.RegionID == RegionID);
                    OnPropertyChanged(nameof(WoredaList));
                }
            }
        }
        public List<Woreda> WoredaList { get; set; }
        private Woreda _selectedWoreda { get; set; }
        public Woreda SelectedWoreda
        {
            get => _selectedWoreda;
            set
            {
                if (_selectedWoreda != value)
                {
                    _selectedWoreda = value;
                    WoredaID = SelectedWoreda.WoredaID;
                    //update the Kebele list
                    KebeleList = App.Database
                                    .GetTableRows<Kebele>("Kebele")
                                    .FindAll(k => k.WoredaID == WoredaID);

                    OnPropertyChanged(nameof(KebeleList));
                }
            }
        }
        public List<Kebele> KebeleList { get; set; }
        private Kebele _selectedKebele { get; set; }
        public Kebele SelectedKebele
        {
            get => _selectedKebele;
            set
            {
                if (_selectedKebele != value)
                {
                    _selectedKebele = value;
                    KebeleID = SelectedKebele.KebeleID;
                }
            }
        }
        public List<string> CBHIMembershipOptions
        {
            get
            {
                return new List<string> { "-", "NO", "YES" };
            }

            set
            {
                OnPropertyChanged(nameof(CBHIMembershipOptions));
            }
        }
        private string _selectedMembershipOption { get; set; }
        public string SelectedMembershipOption
        {
            get => _selectedMembershipOption;
            set
            {
                if (_selectedMembershipOption != value)
                {
                    _selectedMembershipOption = value;
                }
            }
        }
        public List<string> SocialWorkersList { get; set; }
        public List<GeneralOptions> PregantOrLactatingOptions { get; set; }
        public List<NutritionalStatus> NutritionalStatusOptions { get; set; }

        private NutritionalStatus _selectedNutritionStatus { get; set; }
        public NutritionalStatus SelectedNutritionStatus
        {
            get => _selectedNutritionStatus;
            set
            {
                if (_selectedNutritionStatus != value)
                {
                    _selectedNutritionStatus = value;
                    NutritionalStatusPLW = SelectedNutritionStatus.ID;
                }
            }
        }

        public List<YesNo> YesNoOptions { get; set; }
        private YesNo _selectedRisk { get; set; }
        public YesNo SelectedRisk
        {
            get => _selectedRisk;
            set
            {
                if (_selectedRisk != value)
                {
                    _selectedRisk = value;
                    ChildProtectionRisk = SelectedRisk.ID;
                }
            }
        }
    }
}
