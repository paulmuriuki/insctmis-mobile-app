﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class ComplianceHouseholdsListViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public ComplianceHouseholdsListViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            DownloadedTDSComplianceHouseholds.ReplaceRange(GetTDSHouseholdsReadyForCompliance());
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;

        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        public ObservableCollection<ComplianceTDS> GetTDSHouseholdsReadyForCompliance()
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTable("ComplianceTDS");
                var hh = new ObservableCollection<ComplianceTDS>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDS)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Households",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<ComplianceTDS> DownloadedTDSComplianceHouseholds { get; } = new ObservableRangeCollection<ComplianceTDS>();
        private ComplianceTDS selectedTDSHouseholdCommand;
        public ComplianceTDS SelectedTDSHouseholdCommand
        {
            get { return selectedTDSHouseholdCommand; }
            set
            {
                selectedTDSHouseholdCommand = value;
                OnPropertyChanged();
                if (selectedTDSHouseholdCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceHouseholdMembersListPage(selectedTDSHouseholdCommand.ProfileDSHeaderID));

                selectedTDSHouseholdCommand = null;
            }
        }



    }
}
