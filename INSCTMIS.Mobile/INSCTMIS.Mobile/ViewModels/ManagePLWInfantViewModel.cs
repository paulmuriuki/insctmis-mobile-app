﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Services;
using INSCTMIS.Mobile.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class ManagePLWInfantViewModel : LocalBaseViewModel
    {
        private DataStore db;
        private GeneralServices generalServices;
        private readonly IValidator _validator;

        public ManagePLWInfantViewModel(INavigation navigation, int PWLId): base(navigation)
        {
            db = App.Database;
            generalServices = new GeneralServices();
            LoadOptions(PWLId);
            _validator = new InfantValidator();
            AddNewInfantCommand = new Command<string>(SaveInfant);
        }

        void LoadOptions(int PWLId)
        {
            Form1BId = PWLId;
            GenderOptions = generalServices.GetGenders();
            NutritionalStatusOptions = generalServices.GetNutritionalStatus();
        }

        async void SaveInfant(string commandParam)
        {
            bool AddandExit = Convert.ToBoolean(commandParam);
            IsBusy = true;
            Message = "Validating infant data... ";

            var validationResult = _validator.Validate(this);

            if (!validationResult.IsValid)
            {
                ValidateMessage = GetErrorListFromValidationResult(validationResult);
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Infant Registration Error(s)",
                    Message = ValidateMessage,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }

            try
            {
                Infant infant = new Infant()
                {
                    Form1BId = Form1BId,
                    BabyDateOfBirth = BabyDateOfBirth,
                    BabyName = BabyName,
                    BabySex = BabySex,
                    NutritionalStatusInfant = NutritionalStatusInfant
                };

                db.Create(infant);

                if (AddandExit)
                {
                    //Remove hosehold create page from stack
                    MessagingCenter.Send(this, "Add Infant");
                    await Navigation.PopAsync();
                }
                else
                {
                    ResetValues();
                    MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Added New Infant.",
                        Message = "New infant added successfully. Scroll up to add another household member",
                        Cancel = "OK"
                    });
                }

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to save new infant.  ",
                    Message = ex.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        void ResetValues()
        {
            BabyDateOfBirth = "";
            OnPropertyChanged(nameof(BabyDateOfBirth));
            BabyName = "";
            OnPropertyChanged(nameof(BabyName));
            BabySex = "";
            OnPropertyChanged(nameof(BabySex));
            _selectedGender = null;
            OnPropertyChanged(nameof(SelectedGender));
            NutritionalStatusInfant = "";
            OnPropertyChanged(nameof(NutritionalStatusInfant));
            _selectedNutritionStatus = null;
            OnPropertyChanged(nameof(SelectedNutritionStatus));
        }

        public Command AddNewInfantCommand { get; set; }
        public List<Gender> GenderOptions { get; set; }
        public List<NutritionalStatus> NutritionalStatusOptions { get; set; }
        public int Form1BId { get; set; }
        public string BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        private Gender _selectedGender { get; set; }
        public Gender SelectedGender
        {
            get => _selectedGender;
            set
            {
                if (_selectedGender != value)
                {
                    _selectedGender = value;
                    BabySex = SelectedGender.GenderID;
                    OnPropertyChanged(nameof(BabySex));
                }
            }
        }
        public string NutritionalStatusInfant { get; set; }
        private NutritionalStatus _selectedNutritionStatus { get; set; }
        public NutritionalStatus SelectedNutritionStatus
        {
            get => _selectedNutritionStatus;
            set
            {
                if (_selectedNutritionStatus != value)
                {
                    _selectedNutritionStatus = value;
                    NutritionalStatusInfant = SelectedNutritionStatus.ID;
                }
            }
        }
    }
}
