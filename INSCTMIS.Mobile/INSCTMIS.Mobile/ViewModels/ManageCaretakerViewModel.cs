﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Services;
using INSCTMIS.Mobile.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class ManageCaretakerViewModel : LocalBaseViewModel
    {
        public ManageCaretakerViewModel(INavigation navigation) : base(navigation)
        {
            db = App.Database;
            generalServices = new GeneralServices();
            LoadOptions();
            SaveNewCaretakerCommand = new Command(async () => await SaveCaretakerCommand());

            _validator = new CaretakerValidator();
        }

        async Task SaveCaretakerCommand()
        {
            IsBusy = true;
            Message = "Validating Caretaker data... ";

            var validationResult = _validator.Validate(this);

            if (!validationResult.IsValid)
            {
                ValidateMessage = GetErrorListFromValidationResult(validationResult);
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Caretaker Registration Error(s)",
                    Message = ValidateMessage,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }

            try
            {
                long socialWorker = db.GetTableRows<SocialWorker>("SocialWorker")[0].UserId;
                Form1C caretaker = new Form1C()
                {
                    RegionID = RegionID,
                    WoredaID = WoredaID,
                    KebeleID = KebeleID,
                    Gote = Gote,
                    Gare = Gare,
                    CBHIMembership = CBHIMembership,
                    CBHINumber = CBHINumber,
                    CollectionDate = CollectionDate,
                    SocialWorker = socialWorker,
                    CCCCBSPCMember = CCCCBSPCMember,
                    NameOfCareTaker = NameOfCareTaker,
                    HouseHoldIDNumber = HouseHoldIDNumber,
                    CaretakerID = CaretakerID,
                    MalnourishedChildName = MalnourishedChildName,
                    ChildID = ChildID,
                    MalnourishedChildSex = MalnourishedChildSex,
                    ChildDateOfBirth = ChildDateOfBirth,
                    Age = Age,
                    MalnourishmentDegree = MalnourishmentDegree,
                    StartDateTDS = StartDateTDS,
                    NextCNStatusDate = NextCNStatusDate,
                    EndDateTDS = EndDateTDS,
                    DateTypeCertificate = DateTypeCertificate,
                    ChildProtectionRisk = ChildProtectionRisk,
                    Remarks = Remarks
                };

                db.Create(caretaker);
                MessagingCenter.Send(this, "Add Caretaker", caretaker);
                await Navigation.PopAsync(true);
                return;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to save new Caretaker.  ",
                    Message = ex.Message,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }
        }

        void LoadOptions()
        {
            RegionList = db.GetTableRows<Database.Region>("Region");
            YesNoOptions = generalServices.GetYesNo();
            GenderOptions = generalServices.GetGenders();
            NutritionalStatusOptions = generalServices.GetNutritionalStatus();
        }

        public Command SaveNewCaretakerCommand { get; set; }

        private readonly IValidator _validator;

        private DataStore db;
        private GeneralServices generalServices;

        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CollectionDate { get; set; }
        public string CCCCBSPCMember { get; set; } //CCC Member
        public string NameOfCareTaker { get; set; }
        public string HouseHoldIDNumber { get; set; } //PSNP Household ID
        public string CaretakerID { get; set; }
        public string MalnourishedChildName { get; set; }
        public string ChildID { get; set; }
        public string MalnourishedChildSex { get; set; }
        public string ChildDateOfBirth { get; set; }
        public decimal Age { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string StartDateTDS { get; set; } //TSF or CMAM Start Date
        public string NextCNStatusDate { get; set; }
        public string EndDateTDS { get; set; } //End of treatment date
        public string DateTypeCertificate { get; set; } //Malnourished start date
        public string ChildProtectionRisk { get; set; }
        public string Remarks { get; set; }

        public List<Database.Region> RegionList { get; set; }
        private Database.Region _selectedRegion { get; set; }
        public Database.Region SelectedRegion
        {
            get { return _selectedRegion; }
            set
            {
                if (_selectedRegion != value)
                {
                    _selectedRegion = value;
                    RegionID = SelectedRegion.RegionID;
                    //update the Woreda list
                    WoredaList = App.Database
                                    .GetTableRows<Woreda>("Woreda")
                                    .FindAll(w => w.RegionID == RegionID);
                    OnPropertyChanged(nameof(WoredaList));
                }
            }
        }
        public List<Woreda> WoredaList { get; set; }
        private Woreda _selectedWoreda { get; set; }
        public Woreda SelectedWoreda
        {
            get => _selectedWoreda;
            set
            {
                if (_selectedWoreda != value)
                {
                    _selectedWoreda = value;
                    WoredaID = SelectedWoreda.WoredaID;
                    //update the Kebele list
                    KebeleList = App.Database
                                    .GetTableRows<Kebele>("Kebele")
                                    .FindAll(k => k.WoredaID == WoredaID);

                    OnPropertyChanged(nameof(KebeleList));
                }
            }
        }
        public List<Kebele> KebeleList { get; set; }
        private Kebele _selectedKebele { get; set; }
        public Kebele SelectedKebele
        {
            get => _selectedKebele;
            set
            {
                if (_selectedKebele != value)
                {
                    _selectedKebele = value;
                    KebeleID = SelectedKebele.KebeleID;
                }
            }
        }
        public List<string> CBHIMembershipOptions
        {
            get
            {
                return new List<string> { "-", "NO", "YES" };
            }

            set
            {
                OnPropertyChanged(nameof(CBHIMembershipOptions));
            }
        }
        private string _selectedMembershipOption { get; set; }
        public string SelectedMembershipOption
        {
            get => _selectedMembershipOption;
            set
            {
                if (_selectedMembershipOption != value)
                {
                    _selectedMembershipOption = value;
                }
            }
        }
        public List<string> SocialWorkersList { get; set; }
        public List<NutritionalStatus> NutritionalStatusOptions { get; set; }

        private NutritionalStatus _selectedNutritionStatus { get; set; }
        public NutritionalStatus SelectedNutritionStatus
        {
            get => _selectedNutritionStatus;
            set
            {
                if (_selectedNutritionStatus != value)
                {
                    _selectedNutritionStatus = value;
                    MalnourishmentDegree = SelectedNutritionStatus.ID;
                }
            }
        }

        public List<Gender> GenderOptions { get; set; }
        private Gender _selectedGender { get; set; }
        public Gender SelectedGender
        {
            get => _selectedGender;
            set
            {
                if (_selectedGender != value)
                {
                    _selectedGender = value;
                    MalnourishedChildSex = SelectedGender.GenderID;
                    OnPropertyChanged(nameof(MalnourishedChildSex));
                }
            }
        }

        public List<YesNo> YesNoOptions { get; set; }
        private YesNo _selectedRisk { get; set; }
        public YesNo SelectedRisk
        {
            get => _selectedRisk;
            set
            {
                if (_selectedRisk != value)
                {
                    _selectedRisk = value;
                    ChildProtectionRisk = SelectedRisk.ID;
                }
            }
        }
    }
}
