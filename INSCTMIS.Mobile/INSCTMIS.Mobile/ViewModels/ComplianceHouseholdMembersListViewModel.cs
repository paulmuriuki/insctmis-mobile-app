﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using MvvmHelpers;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class ComplianceHouseholdMembersListViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public ComplianceHouseholdMembersListViewModel(INavigation navigation, Guid hhid) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            Navigation = navigation;

            DownloadedTDSComplianceHouseholdMembers.AddRange(GetTDSHouseholdMembersReadyForCompliance(hhid.ToString()));

        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string messageSync;

        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        public ObservableCollection<ComplianceTDSMembers> GetTDSHouseholdMembersReadyForCompliance(string hhid)
        {
            IsBusy = true;
            try
            {
                var items = App.Database.GetTableRows("ComplianceTDSMembers", "ProfileDSHeaderID", hhid, "HasComplied","");
                var hh = new ObservableCollection<ComplianceTDSMembers>();
                foreach (var item in items)
                {
                    var hhitem = (ComplianceTDSMembers)item;
                    hh.Add(hhitem);
                }
                return hh;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error Getting Household Members",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;

            return null;
        }
        public ObservableRangeCollection<ComplianceTDSMembers> DownloadedTDSComplianceHouseholdMembers { get; } = new ObservableRangeCollection<ComplianceTDSMembers>();
        private ComplianceTDS selectedTDSHouseholdMemberCommand;
        public ComplianceTDS SelectedTDSHouseholdMemberCommand
        {
            get { return selectedTDSHouseholdMemberCommand; }
            set
            {
                selectedTDSHouseholdMemberCommand = value;
                OnPropertyChanged();
                if (selectedTDSHouseholdMemberCommand == null)
                    return;
                Navigation.PushAsync(new ComplianceHouseholdMembersListPage(selectedTDSHouseholdMemberCommand.ProfileDSHeaderID));

                selectedTDSHouseholdMemberCommand = null;
            }
        }
    }
}
