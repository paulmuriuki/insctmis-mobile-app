﻿using INSCTMIS.Mobile.Interface;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    class MonitoringViewModel : LocalBaseViewModel
    {
        private IAppClient client;
        public INavigation Navigation;

        public MonitoringViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();

            Form5A1Text = "Form 5A1";
            Form5A2Text = "Form 5A2";
            Form5BText = "Form 5B";
            Form5CText = "Form 5C";
            Navigation = navigation;
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string form5A1Text;
        public string Form5A1Text
        {
            get { return form5A1Text; }
            set { SetProperty(ref form5A1Text, value); }
        }

        private string form5A2Text;
        public string Form5A2Text
        {
            get { return form5A2Text; }
            set { SetProperty(ref form5A2Text, value); }
        }

        private string form5BText;
        public string Form5BText
        {
            get { return form5BText; }
            set { SetProperty(ref form5BText, value); }
        }

        private string form5CText;
        public string Form5CText
        {
            get { return form5CText; }
            set { SetProperty(ref form5CText, value); }
        }

        private string form5DText;
        public string Form5DText
        {
            get { return form5DText; }
            set { SetProperty(ref form5DText, value); }
        }

        private ICommand captureForm5A1Command;
        public ICommand CaptureForm5A1Command => captureForm5A1Command ?? (captureForm5A1Command = new Command(async () => await ExecuteCaptureForm5A1Async()));
        private async Task ExecuteCaptureForm5A1Async()
        {
            if (IsBusy)
            {
                return;
            }

        }

        private ICommand captureForm5A2Command;
        public ICommand CaptureForm5A2Command => captureForm5A2Command ?? (captureForm5A2Command = new Command(async () => await ExecuteCaptureForm5A2Async()));
        private async Task ExecuteCaptureForm5A2Async()
        {
            if (IsBusy)
            {
                return;
            }

        }

        private ICommand captureForm5BCommand;
        public ICommand CaptureForm5BCommand => captureForm5BCommand ?? (captureForm5BCommand = new Command(async () => await ExecuteCaptureForm5BAsync()));
        private async Task ExecuteCaptureForm5BAsync()
        {
            if (IsBusy)
            {
                return;
            }

        }

        private ICommand captureForm5CCommand;
        public ICommand CaptureForm5CCommand => captureForm5CCommand ?? (captureForm5CCommand = new Command(async () => await ExecuteCaptureForm5CAsync()));
        private async Task ExecuteCaptureForm5CAsync()
        {
            if (IsBusy)
            {
                return;
            }

        }

        private ICommand captureForm5DCommand;
        public ICommand CaptureForm5DCommand => captureForm5DCommand ?? (captureForm5DCommand = new Command(async () => await ExecuteCaptureForm5DAsync()));
        private async Task ExecuteCaptureForm5DAsync()
        {
            if (IsBusy)
            {
                return;
            }

        }
    }
}
