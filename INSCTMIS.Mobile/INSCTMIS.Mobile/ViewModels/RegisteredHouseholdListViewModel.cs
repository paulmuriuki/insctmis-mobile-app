﻿using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{

    public class RegisteredHouseholdListViewModel : LocalBaseViewModel
    {
        private DataStore db;
        public string Message { get; set; }

        public ObservableCollection<Form1AViewModel> RegisteredHouseholdList { get; set; }
        private Form1A _selectedHouseHold { get; set; }
        public Form1A SelectedHousehold
        {
            get => _selectedHouseHold;
            set
            {
                if(_selectedHouseHold != value)
                {
                    _selectedHouseHold = value;
                }
            }
        }

        public string PageTitle { get; set; }

        public Command AddHouseholdCommand { get; set; }

        public Command LoadHouseholdsCommand { get; set; }
        public ICommand addMemberCommand { get; set; }
        public ICommand viewMemberCommand { get; set; }
        public RegisteredHouseholdListViewModel() { }
        public RegisteredHouseholdListViewModel(INavigation navigation) : base(navigation) 
        {
            //Instantiate variables needed
            db = App.Database;
            LoadRegisteredHouseholds();
            AddHouseholdCommand = new Command(async () => await Navigation.PushAsync(new ManageHouseholdPage()));
            LoadHouseholdsCommand = new Command(() => LoadRegisteredHouseholds());
            addMemberCommand = new Command(AddHouseholdMember);
            viewMemberCommand = new Command(ViewHouseholdMembers);

            //Subscribe for added households
            MessagingCenter.Subscribe<ManageHouseholdViewModel, Form1A>(this, "Add Household", (obj, item) =>
             {
                 Form1A newHousehold = item as Form1A;
                 var Region = (Database.Region)db.GetTableRow("Region", "RegionID", "" + newHousehold.RegionID);
                 var Woreda = (Woreda)db.GetTableRow("Woreda", "WoredaID", "" + newHousehold.WoredaID);
                 var Kebele = (Kebele)db.GetTableRow("Kebele", "KebeleID", "" + newHousehold.KebeleID);
                 var count = db.GetTableRows<HouseholdMember>("HouseholdMember", "Form1AId", "" + item.Id).Count;

                 RegisteredHouseholdList.Insert(0, new Form1AViewModel() { 
                    Id = newHousehold.Id,
                    Location = string.Concat(Region.RegionName+", ", Woreda.WoredaName + ", ", Kebele.KebeleName),
                    HouseholdIDNumber = newHousehold.HouseholdIDNumber,
                    NameOfHouseHoldHead = newHousehold.NameOfHouseHoldHead,
                    MembersCount = count > 0 ? count + " Member(s)" : "No Member",
                    Household = newHousehold
                 });

                 PageTitle = RegisteredHouseholdList.Count > 0? 
                                String.Concat("Registered Households - ",RegisteredHouseholdList.Count):
                                "Registered Households";
                 OnPropertyChanged(nameof(PageTitle));
             });

            MessagingCenter.Subscribe<ManageHouseholdMemberViewModel>(this, "Add Member", (obj) =>
            {
                LoadRegisteredHouseholds();
                OnPropertyChanged(nameof(RegisteredHouseholdList));
            });
        }

        void LoadRegisteredHouseholds()
        {
            IsBusy = true;
            try
            {
                List<Form1A> households = db.GetTableRows<Form1A>("Form1A");
                RegisteredHouseholdList = new ObservableCollection<Form1AViewModel>();
                foreach (var item in households)
                {
                    var Region = (Database.Region)db.GetTableRow("Region", "RegionID", "" + item.RegionID);
                    var Woreda = (Woreda)db.GetTableRow("Woreda", "WoredaID", "" + item.WoredaID);
                    var Kebele = (Kebele)db.GetTableRow("Kebele", "KebeleID", "" + item.KebeleID);
                    var count = db.GetTableRows<HouseholdMember>("HouseholdMember", "Form1AId", "" + item.Id).Count;

                    RegisteredHouseholdList.Insert(0, new Form1AViewModel()
                    {
                        Id = item.Id,
                        Location = String.Concat(Region.RegionName + ", ", Woreda.WoredaName + ", ", Kebele.KebeleName),
                        HouseholdIDNumber = item.HouseholdIDNumber,
                        NameOfHouseHoldHead = item.NameOfHouseHoldHead,
                        MembersCount = count > 0 ? count + " Member(s)" : "No Member",
                        Household = item
                    }); ;
                }

                PageTitle = RegisteredHouseholdList.Count > 0 ?
                                String.Concat("Registered Households - ", RegisteredHouseholdList.Count) :
                                "Registered Households";
                OnPropertyChanged(nameof(PageTitle));


            }
            catch(Exception e)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to load households.  ",
                    Message = e.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        public ICommand AddMemberCommand
        {
            get => addMemberCommand;
        }
        async void AddHouseholdMember(object sender)
        {
            //Grab the active household from the sender object
            Form1AViewModel model = (Form1AViewModel)sender;
            await Navigation.PushAsync(new ManageHouseholdMembers(model.Id));
        }

        public ICommand ViewMembersCommand
        {
            get => viewMemberCommand;
        }
        async void ViewHouseholdMembers(object sender)
        {
            Form1AViewModel model = (Form1AViewModel)sender;
            await Navigation.PushAsync(new RegisteredHouseholdMembersListPage(model.Id));
        }

    }

    public class Form1AViewModel
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string HouseholdIDNumber { get; set; } //AKA PSNP HH Number
        public string NameOfHouseHoldHead { get; set; }
        public string MembersCount { get; set; }
        public Form1A Household { get; set; }
    }

}
