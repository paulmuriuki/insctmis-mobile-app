﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Pages;
using INSCTMIS.Mobile.Validators;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class ManageHouseholdViewModel : LocalBaseViewModel
    {
        private IAppClient client;

        private DataStore db;

        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string HouseholdIDNumber { get; set; } //AKA PSNP HH Number
        public string NameOfHouseHoldHead { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CollectionDate { get; set; }
        public string Remarks { get; set; }
        public string CCCCBSPCMember { get; set; }
        public List<Database.Region> RegionList { get; set; }
        private Database.Region _selectedRegion { get; set; }
        public Database.Region SelectedRegion
        {
            get { return _selectedRegion; }
            set
            {
                if(_selectedRegion != value)
                {
                    _selectedRegion = value;
                    RegionID = SelectedRegion.RegionID;
                    //update the Woreda list
                    WoredaList = App.Database
                                    .GetTableRows<Woreda>("Woreda")
                                    .FindAll(w => w.RegionID == RegionID);
                    OnPropertyChanged(nameof(WoredaList));
                }
            }
        }
        public List<Woreda> WoredaList { get; set; }
        private Woreda _selectedWoreda { get; set; }
        public Woreda SelectedWoreda
        {
            get => _selectedWoreda;
            set
            {
                if(_selectedWoreda != value)
                {
                    _selectedWoreda = value;
                    WoredaID = SelectedWoreda.WoredaID;
                    //update the Kebele list
                    KebeleList = App.Database
                                    .GetTableRows<Kebele>("Kebele")
                                    .FindAll(k => k.WoredaID == WoredaID);

                    OnPropertyChanged(nameof(KebeleList));
                }
            }
        }
        public List<Kebele> KebeleList { get; set; }
        private Kebele _selectedKebele { get; set; }
        public Kebele SelectedKebele
        {
            get => _selectedKebele;
            set
            {
                if(_selectedKebele != value)
                {
                    _selectedKebele = value;
                    KebeleID = SelectedKebele.KebeleID;
                }
            }
        }
        public List<string> CBHIMembershipOptions { 
            get
            {
                return new List<string> { "-", "NO", "YES" };
            }

            set
            {
                OnPropertyChanged(nameof(CBHIMembershipOptions));
            }
        }
        private string _selectedMembershipOption { get; set; }
        public string SelectedMembershipOption { 
            get => _selectedMembershipOption;
            set
            {
                if(_selectedMembershipOption != value)
                {
                    _selectedMembershipOption = value;
                }
            } 
        }
        public List<string> SocialWorkersList { get; set; }
        

        public Command SaveNewHouseholdCommand { get; set; }

        private readonly IValidator _validator;

        public ManageHouseholdViewModel(INavigation navigation) : base(navigation)
        {
            db = App.Database;
            LoadOptions();
            SaveNewHouseholdCommand = new Command(async () => await SaveHouseholdCommand());

            _validator = new HouseholdValidator();
        }

        async Task SaveHouseholdCommand()
        {
            IsBusy = true;
            Message = "Validating household data... ";

            var validationResult = _validator.Validate(this);

            if (!validationResult.IsValid)
            {
                ValidateMessage = GetErrorListFromValidationResult(validationResult);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Household Registration Error(s)",
                    Message = ValidateMessage,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }

            try
            {
                Form1A household = new Form1A()
                {
                    RegionID = RegionID,
                    WoredaID = WoredaID,
                    KebeleID = KebeleID,
                    Gote = Gote,
                    Gare = Gare,
                    HouseholdIDNumber = HouseholdIDNumber,
                    NameOfHouseHoldHead = NameOfHouseHoldHead,
                    CBHIMembership = CBHIMembership,
                    CBHINumber = CBHINumber,
                    CollectionDate = CollectionDate,
                    SocialWorker = db.GetTableRows<SocialWorker>("SocialWorker")[0].UserId,
                    Remarks = Remarks,
                    CCCCBSPCMember = CCCCBSPCMember
                };

                db.Create(household);
                MessagingCenter.Send(this, "Add Household", household);
                await Navigation.PopAsync(true);
                IsBusy = false;
                return;
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to save new household.  ",
                    Message = ex.Message,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }
        }

        void LoadOptions()
        {
            RegionList = db.GetTableRows<Database.Region>("Region");
        }
    }
}
