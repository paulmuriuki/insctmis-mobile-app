﻿using FluentValidation;
using FormsToolkit;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Services;
using INSCTMIS.Mobile.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class ManageHouseholdMemberViewModel: LocalBaseViewModel
    {
        private DataStore db;
        private GeneralServices generalServices;

        public ManageHouseholdMemberViewModel(INavigation navigation) : base(navigation)
        {
            db = App.Database;
            generalServices = new GeneralServices();
            LoadOptions();
            _validator = new HouseholdMemberValidator();
        }

        public ManageHouseholdMemberViewModel(INavigation navigation, int HouseholdId) : base(navigation)
        {
            db = App.Database;
            generalServices = new GeneralServices();
            LoadOptions(HouseholdId);
            _validator = new HouseholdMemberValidator();
            AddNewMemberCommand = new Command<string>(SaveHouseholdMember);
        }

        void LoadOptions()
        {
            YesNoOptions = generalServices.GetYesNo();
            GenderOptions = generalServices.GetGenders();
            SchoolGradeOptions = generalServices.GetSchoolGrade();
            NutritionalStatusOptions = generalServices.GetNutritionalStatus();
        }
        void LoadOptions(int HouseholdId)
        {
            Form1AId = HouseholdId;
            YesNoOptions = generalServices.GetYesNo();
            GenderOptions = generalServices.GetGenders();
            SchoolGradeOptions = generalServices.GetSchoolGrade();
            NutritionalStatusOptions = generalServices.GetNutritionalStatus();
        }

        async void SaveHouseholdMember(string commandParam)
        {
            bool AddandExit = Convert.ToBoolean(commandParam);
            IsBusy = true;
            Message = "Validating household member data... ";

            var validationResult = _validator.Validate(this);

            if (!validationResult.IsValid)
            {
                ValidateMessage = GetErrorListFromValidationResult(validationResult);
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Household Member Registration Error(s)",
                    Message = ValidateMessage,
                    Cancel = "OK"
                });
                IsBusy = false;
                return;
            }

            try {
                HouseholdMember member = new HouseholdMember()
                {
                    Form1AId = Form1AId,
                    HouseHoldMemberName = HouseHoldMemberName,
                    IndividualID = IndividualID,
                    DateOfBirth = DateOfBirth,
                    Age = Age,
                    Sex = Sex,
                    Pregnant = Pregnant,
                    Lactating = Lactating,
                    Handicapped = Handicapped,
                    ChronicallyIll = ChronicallyIll,
                    NutritionalStatus = NutritionalStatus,
                    childUnderTSForCMAM = ChildUnderTSForCMAM,
                    EnrolledInSchool = EnrolledInSchool,
                    Grade = Grade,
                    SchoolName = SchoolName,
                    ChildProtectionRisk = ChildProtectionRisk
                };


                db.Create(member);

                if (AddandExit)
                {
                    //Remove hosehold create page from stack
                    MessagingCenter.Send(this, "Add Member");
                    await Navigation.PopAsync();
                }
                else
                {
                    ResetValues();
                    MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Added New Household Member.",
                        Message = "New household member added successfully. Scroll up to add another household member",
                        Cancel = "OK"
                    });
                }

            }catch(Exception ex) 
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to save new household member.  ",
                    Message = ex.Message,
                    Cancel = "OK"
                });
            } finally
            {
                IsBusy = false;
            }
        }

        void ResetValues()
        {
            HouseHoldMemberName = "";
            OnPropertyChanged(nameof(HouseHoldMemberName));
            IndividualID = "";
            OnPropertyChanged(nameof(IndividualID));
            DateOfBirth = "";
            OnPropertyChanged(nameof(DateOfBirth));
            Age = "";
            OnPropertyChanged(nameof(Age));
            Sex = "";
            OnPropertyChanged(nameof(Sex));
            _selectedGender = null;
            OnPropertyChanged(nameof(SelectedGender));
            Pregnant = "";
            OnPropertyChanged(nameof(Pregnant));
            _selectedPregnancy = null;
            OnPropertyChanged(nameof(SelectedPregnancy));
            Lactating = "";
            OnPropertyChanged(nameof(Lactating));
            _selectedLactate = null;
            OnPropertyChanged(nameof(SelectedLactate));
            Handicapped = "";
            OnPropertyChanged(nameof(Handicapped));
            _selectedHandicap = null;
            OnPropertyChanged(nameof(SelectedHandicap));
            ChronicallyIll = "";
            OnPropertyChanged(nameof(ChronicallyIll));
            _selectedChronic = null;
            OnPropertyChanged(nameof(SelectedChronic));
            NutritionalStatus = "";
            OnPropertyChanged(nameof(NutritionalStatus));
            _selectedNutritionStatus = null;
            OnPropertyChanged(nameof(SelectedNutritionStatus));
            ChildUnderTSForCMAM = "";
            OnPropertyChanged(nameof(ChildUnderTSForCMAM));
            _selectedChildCMAM = null;
            OnPropertyChanged(nameof(SelectedChildCMAM));
            EnrolledInSchool = "";
            OnPropertyChanged(nameof(EnrolledInSchool));
            _selectedEnrolment = null;
            OnPropertyChanged(nameof(SelectedEnrolment));
            Grade = "";
            OnPropertyChanged(nameof(Grade));
            _selectedGrade = null;
            OnPropertyChanged(nameof(SelectedGrade));
            SchoolName = "";
            OnPropertyChanged(nameof(SchoolName));
            ChildProtectionRisk = "";
            OnPropertyChanged(nameof(ChildProtectionRisk));
            _selectedRisk = null;
            OnPropertyChanged(nameof(SelectedRisk));

        }
        public Command AddNewMemberCommand { get; set; }
        
        private readonly IValidator _validator;
        public List<YesNo> YesNoOptions { get; set; }
        public List<Gender> GenderOptions { get; set; }
        public List<SchoolGrade> SchoolGradeOptions { get; set; }
        public List<NutritionalStatus> NutritionalStatusOptions { get; set; }
        public int Form1AId { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        private Gender _selectedGender { get; set; }
        public Gender SelectedGender
        {
            get => _selectedGender;
            set
            {
                if(_selectedGender != value)
                {
                    _selectedGender = value;
                    Sex = SelectedGender.GenderID;
                    OnPropertyChanged(nameof(Sex));
                }
            }
        }
        public string Pregnant { get; set; }
        private YesNo _selectedPregnancy { get; set; }
        public YesNo SelectedPregnancy
        {
            get => _selectedPregnancy;
            set
            {
                if(_selectedPregnancy != value)
                {
                    _selectedPregnancy = value;
                    Pregnant = SelectedPregnancy.ID;
                }
            }
        }
        public string Lactating { get; set; }
        private YesNo _selectedLactate { get; set; }
        public YesNo SelectedLactate
        {
            get => _selectedLactate;
            set
            {
                if (_selectedLactate != value)
                {
                    _selectedLactate = value;
                    Lactating = SelectedLactate.ID;
                }
            }
        }
        public string Handicapped { get; set; }
        private YesNo _selectedHandicap { get; set; }
        public YesNo SelectedHandicap
        {
            get => _selectedHandicap;
            set
            {
                if (_selectedHandicap != value)
                {
                    _selectedHandicap = value;
                    Handicapped = SelectedHandicap.ID;
                }
            }
        }
        public string ChronicallyIll { get; set; }
        private YesNo _selectedChronic { get; set; }
        public YesNo SelectedChronic
        {
            get => _selectedChronic;
            set
            {
                if (_selectedChronic != value)
                {
                    _selectedChronic = value;
                    ChronicallyIll = SelectedChronic.ID;
                }
            }
        }
        public string NutritionalStatus { get; set; }
        private NutritionalStatus _selectedNutritionStatus { get; set; }
        public NutritionalStatus SelectedNutritionStatus
        {
            get => _selectedNutritionStatus;
            set
            {
                if (_selectedNutritionStatus != value)
                {
                    _selectedNutritionStatus = value;
                    NutritionalStatus = SelectedNutritionStatus.ID;
                }
            }
        }
        public string ChildUnderTSForCMAM { get; set; }
        private YesNo _selectedChildCMAM { get; set; }
        public YesNo SelectedChildCMAM
        {
            get => _selectedChildCMAM;
            set
            {
                if (_selectedChildCMAM != value)
                {
                    _selectedChildCMAM = value;
                    ChildUnderTSForCMAM = SelectedChildCMAM.ID;
                }
            }
        }
        public string EnrolledInSchool { get; set; }
        private YesNo _selectedEnrolment { get; set; }
        public YesNo SelectedEnrolment
        {
            get => _selectedEnrolment;
            set
            {
                if (_selectedEnrolment != value)
                {
                    _selectedEnrolment = value;
                    EnrolledInSchool = SelectedEnrolment.ID;
                }
            }
        }
        public string Grade { get; set; }
        private SchoolGrade _selectedGrade { get; set; }
        public SchoolGrade SelectedGrade
        {
            get => _selectedGrade;
            set
            {
                if (_selectedGrade != value)
                {
                    _selectedGrade = value;
                    Grade = SelectedGrade.ID;
                }
            }
        }
        public string SchoolName { get; set; }
        public string ChildProtectionRisk { get; set; }
        private YesNo _selectedRisk { get; set; }
        public YesNo SelectedRisk
        {
            get => _selectedRisk;
            set
            {
                if (_selectedRisk != value)
                {
                    _selectedRisk = value;
                    ChildProtectionRisk = SelectedRisk.ID;
                }
            }
        }
    }
}
