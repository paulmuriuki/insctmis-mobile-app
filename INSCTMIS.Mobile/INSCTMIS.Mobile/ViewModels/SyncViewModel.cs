﻿using FormsToolkit;
using INSCTMIS.Mobile.Converters;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Helpers;
using INSCTMIS.Mobile.Interface;
using INSCTMIS.Mobile.Models;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.ViewModels
{
    public class SyncViewModel : LocalBaseViewModel
    {
        #region Properties

        private IAppClient client;
        public INavigation Navigation;

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        public SyncViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            email = Settings.Current.Email;
            password = Settings.Current.Password;

            SyncedDownload = Settings.Current.LastSyncDown;
            SyncedUpload = Settings.Current.LastSync;

            SyncDownRegistrationText = "Download Data from Server";
            SyncUpForm1AText = "Upload Form 1A Data To Server";
            SyncUpForm1BText = "Upload Form 1B Data To Server";
            SyncUpForm1CText = "Upload Form 1C Data To Server";

            SyncDownComplianceText = "Download Data from Server";
            SyncUpComplianceText = "Upload Data To Server";

            SyncDownMonitoringText = "Download Data from Server";
            SyncUpMonitoringText = "Upload Data To Server";

            SyncDownRetargetingText = "Download Data from Server";
            SyncUpRetargetingText = "Upload Data To Server";

            Navigation = navigation;
        }

        private int localListing;

        public int LocalListing
        {
            get { return localListing; }
            set { SetProperty(ref localListing, value); }
        }

        private DateTime syncedUpload;

        public DateTime SyncedUpload
        {
            get { return syncedUpload; }
            set { SetProperty(ref syncedUpload, value); }
        }

        private DateTime syncedDownload;

        public DateTime SyncedDownload
        {
            get { return syncedDownload; }
            set { SetProperty(ref syncedDownload, value); }
        }

        #endregion Properties

        #region Sync

        private string syncUpForm1AText;
        public string SyncUpForm1AText
        {
            get { return syncUpForm1AText; }
            set { SetProperty(ref syncUpForm1AText, value); }
        }

        private string syncUpForm1BText;
        public string SyncUpForm1BText
        {
            get { return syncUpForm1BText; }
            set { SetProperty(ref syncUpForm1BText, value); }
        }

        private string syncUpForm1CText;
        public string SyncUpForm1CText
        {
            get { return syncUpForm1CText; }
            set { SetProperty(ref syncUpForm1CText, value); }
        }

        private string syncDownRegistrationText;
        public string SyncDownRegistrationText
        {
            get { return syncDownRegistrationText; }
            set { SetProperty(ref syncDownRegistrationText, value); }
        }


        private string syncUpComplianceText;
        public string SyncUpComplianceText
        {
            get { return syncUpComplianceText; }
            set { SetProperty(ref syncUpComplianceText, value); }
        }


        private string syncDownComplianceText;
        public string SyncDownComplianceText
        {
            get { return syncDownComplianceText; }
            set { SetProperty(ref syncDownComplianceText, value); }
        }

        private string syncUpMonitoringText;
        public string SyncUpMonitoringText
        {
            get { return syncUpComplianceText; }
            set { SetProperty(ref syncUpComplianceText, value); }
        }

        private string syncDownMonitoringText;
        public string SyncDownMonitoringText
        {
            get { return syncDownMonitoringText; }
            set { SetProperty(ref syncDownMonitoringText, value); }
        }


        private string syncUpRetargetingText;
        public string SyncUpRetargetingText
        {
            get { return syncUpRetargetingText; }
            set { SetProperty(ref syncUpRetargetingText, value); }
        }

        private string syncDownRetargetingText;
        public string SyncDownRetargetingText
        {
            get { return syncDownRetargetingText; }
            set { SetProperty(ref syncDownRetargetingText, value); }
        }

        private string logRegistration;

        public string LogRegistration
        {
            get { return logRegistration; }
            set { SetProperty(ref logRegistration, value); }
        }

        private string messageSync;

        public string MessageSync
        {
            get { return messageSync; }
            set { SetProperty(ref messageSync, value); }
        }

        private ICommand syncUpForm1ACommand;
        public ICommand SyncUpForm1ACommand => syncUpForm1ACommand ?? (syncUpForm1ACommand = new Command(async () => await ExecuteSyncUpForm1AAsync()));
        private async Task ExecuteSyncUpForm1AAsync()
        {
            
            if (!CrossConnectivity.Current.IsConnected)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sync Failed",
                    Message = "Uh Oh, It looks like you have gone offline. \n" +
                              "Please check your internet connection and try again.",
                    Cancel = "OK"
                });
                return;
            }
            IsBusy = true;
            SyncUpForm1AText = "Uploading data...";
            OnPropertyChanged(nameof(SyncUpForm1AText));

            var model = new LocalDeviceInfo()
            {
                Version = CrossDeviceInfo.Current.Version,
                AppBuild = CrossDeviceInfo.Current.AppBuild,
                AppVersion = CrossDeviceInfo.Current.AppVersion,
                DeviceName = CrossDeviceInfo.Current.DeviceName,
                DeviceId = CrossDeviceInfo.Current.Id,
                Idiom = CrossDeviceInfo.Current.Idiom,
                IsDevice = CrossDeviceInfo.Current.IsDevice,
                DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                DeviceModel = CrossDeviceInfo.Current.Model,
                Platform = CrossDeviceInfo.Current.Platform,
                VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
            };

            List<Form1A> householdList = App.Database.GetTableRows<Form1A>("Form1A");
            if(householdList.Count > 0)
            {
                int uploadCounter = 0;
                foreach (var household in householdList)
                {
                    List<HouseholdMember> members =
                        App.Database.GetTableRows<HouseholdMember>("HouseholdMember", "Form1AId", "" + household.Id);

                    household.HouseholdDetails = members;
                    household.KebeleId = new KebeleId(); 
                    household.KebeleId.KebeleID = household.KebeleID;

                    //send request
                    ApiStatus feedback = await client.UploadForm1AData(household, model);

                    //review feedback
                    switch (feedback.StatusId)
                    {
                        case 0:

                            //Delete all household members
                            foreach (var item in members)
                            {
                                App.Database.Delete<HouseholdMember>(item.Id);
                            }

                            //Delete household
                            App.Database.Delete<Form1A>(household.Id);

                            uploadCounter ++;
                            break;
                    }
                }

                MessageSync = string.Empty;
                SyncUpForm1AText = "Upload Form 1A Data To Server";
                IsBusy = false;

                string uploadFeedback = uploadCounter != householdList.Count ?
                    "" + uploadCounter + "/" + householdList.Count + " households uploaded. The session might have timed out during upload. Try again later."
                    : "" + uploadCounter + "/" + householdList.Count + " households uploaded successfully";

                MessagingService.Current.SendMessage(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Upload status",
                            Message = uploadFeedback,
                            Cancel = "OK"
                        });
            }
            else
            {
                MessageSync = string.Empty;
                SyncUpForm1AText = "Upload Form 1A Data To Server";
                IsBusy = false;

                MessagingService.Current.SendMessage(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Note",
                            Message = "No households ready for uploading.",
                            Cancel = "OK"
                        });
            }

        }

        private ICommand syncUpForm1BCommand;
        public ICommand SyncUpForm1BCommand => syncUpForm1BCommand ?? (syncUpForm1BCommand = new Command(async () => await ExecuteSyncUpForm1BAsync()));
        private async Task ExecuteSyncUpForm1BAsync()
        {

            if (!CrossConnectivity.Current.IsConnected)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sync Failed",
                    Message = "Uh Oh, It looks like you have gone offline. \n" +
                              "Please check your internet connection and try again.",
                    Cancel = "OK"
                });
                return;
            }
            IsBusy = true;
            SyncUpForm1BText = "Uploading data...";
            OnPropertyChanged(nameof(SyncUpForm1BText));

            var model = new LocalDeviceInfo()
            {
                Version = CrossDeviceInfo.Current.Version,
                AppBuild = CrossDeviceInfo.Current.AppBuild,
                AppVersion = CrossDeviceInfo.Current.AppVersion,
                DeviceName = CrossDeviceInfo.Current.DeviceName,
                DeviceId = CrossDeviceInfo.Current.Id,
                Idiom = CrossDeviceInfo.Current.Idiom,
                IsDevice = CrossDeviceInfo.Current.IsDevice,
                DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                DeviceModel = CrossDeviceInfo.Current.Model,
                Platform = CrossDeviceInfo.Current.Platform,
                VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
            };

            List<Form1B> pregnantWomenList = App.Database.GetTableRows<Form1B>("Form1B");
            if (pregnantWomenList.Count > 0)
            {
                int uploadCounter = 0;
                foreach (var pregnantWoman in pregnantWomenList)
                {
                    List<Infant> infants =
                        App.Database.GetTableRows<Infant>("Infant", "Form1BId", "" + pregnantWoman.Id);

                    pregnantWoman.HouseholdDetails = infants;
                    pregnantWoman.KebeleId = new KebeleId();
                    pregnantWoman.KebeleId.KebeleID = pregnantWoman.KebeleID;

                    //send request
                    ApiStatus feedback = await client.UploadForm1BData(pregnantWoman, model);

                    //review feedback
                    switch (feedback.StatusId)
                    {
                        case 0:

                            //Delete all babies
                            foreach (var item in infants)
                            {
                                App.Database.Delete<Infant>(item.Id);
                            }

                            //Delete mother
                            App.Database.Delete<Form1B>(pregnantWoman.Id);

                            uploadCounter++;
                            break;
                    }
                }

                MessageSync = string.Empty;
                SyncUpForm1BText = "Upload Form 1B Data To Server";
                IsBusy = false;

                string uploadFeedback = uploadCounter != pregnantWomenList.Count ?
                    "" + uploadCounter + "/" + pregnantWomenList.Count + " PLW records uploaded. The session might have timed out during upload. Try again later."
                    : "" + uploadCounter + "/" + pregnantWomenList.Count + " PLW records uploaded successfully";

                MessagingService.Current.SendMessage(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Upload status",
                            Message = uploadFeedback,
                            Cancel = "OK"
                        });
            }
            else
            {
                MessageSync = string.Empty;
                SyncUpForm1BText = "Upload Form 1B Data To Server";
                IsBusy = false;

                MessagingService.Current.SendMessage(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Note",
                            Message = "No PLW data ready for uploading.",
                            Cancel = "OK"
                        });
            }

        }

        private ICommand syncUpForm1CCommand;
        public ICommand SyncUpForm1CCommand => syncUpForm1CCommand ?? (syncUpForm1CCommand = new Command(async () => await ExecuteSyncUpForm1CAsync()));
        private async Task ExecuteSyncUpForm1CAsync()
        {

            if (!CrossConnectivity.Current.IsConnected)
            {
                MessagingService.Current.SendMessage(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sync Failed",
                    Message = "Uh Oh, It looks like you have gone offline. \n" +
                              "Please check your internet connection and try again.",
                    Cancel = "OK"
                });
                return;
            }
            IsBusy = true;
            SyncUpForm1CText = "Uploading data...";
            OnPropertyChanged(nameof(SyncUpForm1BText));

            var model = new LocalDeviceInfo()
            {
                Version = CrossDeviceInfo.Current.Version,
                AppBuild = CrossDeviceInfo.Current.AppBuild,
                AppVersion = CrossDeviceInfo.Current.AppVersion,
                DeviceName = CrossDeviceInfo.Current.DeviceName,
                DeviceId = CrossDeviceInfo.Current.Id,
                Idiom = CrossDeviceInfo.Current.Idiom,
                IsDevice = CrossDeviceInfo.Current.IsDevice,
                DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                DeviceModel = CrossDeviceInfo.Current.Model,
                Platform = CrossDeviceInfo.Current.Platform,
                VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
            };

            List<Form1C> caretakerlist = App.Database.GetTableRows<Form1C>("Form1C");
            if (caretakerlist.Count > 0)
            {
                int uploadCounter = 0;
                foreach (var caretaker in caretakerlist)
                {
                    caretaker.KebeleId = new KebeleId();
                    caretaker.KebeleId.KebeleID = caretaker.KebeleID;
                    //send request
                    ApiStatus feedback = await client.UploadForm1CData(caretaker, model);

                    //review feedback
                    switch (feedback.StatusId)
                    {
                        case 0:

                            //Delete caretaker
                            App.Database.Delete<Form1C>(caretaker.Id);

                            uploadCounter++;
                            break;
                    }
                }

                MessageSync = string.Empty;
                SyncUpForm1CText = "Upload Form 1C Data To Server";
                IsBusy = false;

                string uploadFeedback = uploadCounter != caretakerlist.Count ?
                    "" + uploadCounter + "/" + caretakerlist.Count + " Caretaker records uploaded. The session might have timed out during upload. Try again later."
                    : "" + uploadCounter + "/" + caretakerlist.Count + " Caretaker records uploaded successfully";

                MessagingService.Current.SendMessage(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Upload status",
                            Message = uploadFeedback,
                            Cancel = "OK"
                        });
            }
            else
            {
                MessageSync = string.Empty;
                SyncUpForm1CText = "Upload Form 1C Data To Server";
                IsBusy = false;

                MessagingService.Current.SendMessage(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Note",
                            Message = "No Caretaker data ready for uploading.",
                            Cancel = "OK"
                        });
            }

        }

        private ICommand syncDownComplianceAsyncCommand;
        public ICommand SyncDownComplianceAsyncCommand => syncDownComplianceAsyncCommand ?? (syncDownComplianceAsyncCommand = new Command(async () => await ExecuteSyncDownComplianceAsync()));
        private async Task ExecuteSyncDownComplianceAsync()
        {
            if (IsBusy)
            {
                return;
            }

            QueryParameter queryParameter = new QueryParameter
            {
                KebeleID = "1020",
                ReportingPeriodID = "49"
            };
                
            ComplianceResponse complianceResponse = null;
            MessageSync = "Pulling data from the server... ...";
            SyncDownComplianceText = "Downloading Data . . .";


            try
            {
                complianceResponse = await client.GetHouseholdsReadyForCompliance(queryParameter);

                foreach (var item in complianceResponse.TDSHouseholds)
                {
                    App.Database.AddOrUpdate(item);
                }

                foreach (var item in complianceResponse.TDSHouseholdMembers)
                {
                    App.Database.AddOrUpdate(item);
                }

                //foreach (var item in complianceResponse.TDSCMCHouseholdDetails)
                //{
                //    App.Database.AddOrUpdate(item);
                //}

                //foreach (var item in complianceResponse.CPHouseholdDetails)
                //{
                //    App.Database.AddOrUpdate(item);
                //}

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Success",
                    Message = "Data downloaded successfully",
                    Cancel = "OK"
                });
            }
            catch (Exception)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                SyncDownComplianceText = "Download Data from Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }

        private ICommand syncUpMonitoringCommand;
        public ICommand SyncUpMonitoringCommand => syncUpMonitoringCommand ?? (syncUpMonitoringCommand = new Command(async () => await ExecuteSyncUpMonitoringAsync()));
        private async Task ExecuteSyncUpMonitoringAsync()
        {
            if (IsBusy)
            {
                return;
            }

        }

        private ICommand syncDownMonitoringCommand;
        public ICommand SyncDownMonitoringCommand => syncDownMonitoringCommand ?? (syncDownMonitoringCommand = new Command(async () => await ExecuteSyncDownMonitoringAsync()));
        private async Task ExecuteSyncDownMonitoringAsync()
        {
            if (IsBusy)
            {
                return;
            }

            QueryParameter queryParameter = new QueryParameter
            {
                KebeleID = "1020",
                ReportingPeriodID = "49"
            };

            MonitoringResponse monitoringResponse = null;
            MessageSync = "Pulling data from the server... ...";
            SyncDownMonitoringText = "Downloading Data . . .";

            try
            {
                monitoringResponse = await client.GetHouseholdsReadyForMonitoring(queryParameter);

                if(monitoringResponse.TDS1HouseholdDetails != null)
                {
                    foreach (var item in monitoringResponse.TDS1HouseholdDetails)
                    {
                        App.Database.AddOrUpdate(item);
                    }

                }

                if (monitoringResponse.TDS2HouseholdDetails != null)
                {
                    foreach (var item in monitoringResponse.TDS2HouseholdDetails)
                    {
                        App.Database.AddOrUpdate(item);
                    }

                }

                if (monitoringResponse.PLWHouseholdDetails != null)
                {
                    foreach (var item in monitoringResponse.PLWHouseholdDetails)
                    {
                        App.Database.AddOrUpdate(item);
                    }

                }

                if (monitoringResponse.CMCHouseholdDetails != null)
                {
                    foreach (var item in monitoringResponse.CMCHouseholdDetails)
                    {
                        App.Database.AddOrUpdate(item);
                    }

                }

                if (monitoringResponse.CPHouseholdDetails != null)
                {
                    foreach (var item in monitoringResponse.CPHouseholdDetails)
                    {
                        App.Database.AddOrUpdate(item);
                    }

                }

                if (monitoringResponse.CPCaseDetails != null)
                {
                    foreach (var item in monitoringResponse.CPCaseDetails)
                    {
                        var data = monitoringResponse.CPHouseholdDetails.Where(x => x.ChildProtectionHeaderId == item.ChildProtectionHeaderId);
                        if (data != null)
                        {
                            App.Database.AddOrUpdate(item);
                        }
                    }

                }

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Success",
                    Message = "Data downloaded successfully",
                    Cancel = "OK"
                });



            }
            catch (Exception)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                SyncDownMonitoringText = "Download Data from Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }


        private ICommand syncUpRetargetingCommand;
        public ICommand SyncUpRetargetingCommand => syncUpRetargetingCommand ?? (syncUpRetargetingCommand = new Command(async () => await ExecuteSyncUpRetargetingAsync()));
        private async Task ExecuteSyncUpRetargetingAsync()
        {
            if (IsBusy)
            {
                return;
            }

        }

        private ICommand syncDownRetargetingCommand;
        public ICommand SyncDownRetargetingCommand => syncDownRetargetingCommand ?? (syncDownRetargetingCommand = new Command(async () => await ExecuteSyncDownRetargetingAsync()));
        private async Task ExecuteSyncDownRetargetingAsync()
        {
            if (IsBusy)
            {
                return;
            }

            QueryParameter2 queryParameter = new QueryParameter2
            {
                KebeleID = "1020",
                FiscalYear = "20192020"
            };

            RetargetingResponse retargetingResponse = null;
            MessageSync = "Pulling data from the server... ...";
            SyncDownRetargetingText = "Downloading Data . . .";

            try
            {
                retargetingResponse = await client.GetHouseholdsReadyForRetargeting(queryParameter);

                if (retargetingResponse.RetargetingHousehold != null)
                {
                    foreach (var item in retargetingResponse.RetargetingHousehold)
                    {
                        App.Database.AddOrUpdate(item);
                    }
                }

                if (retargetingResponse.RetargetingHouseholdDetails != null)
                {
                    foreach (var item in retargetingResponse.RetargetingHouseholdDetails)
                    {
                        var data = retargetingResponse.RetargetingHousehold.Where(x => x.RetargetingHeaderID == item.RetargetingHeaderID);
                        if(data != null)
                        {
                            App.Database.AddOrUpdate(item);
                        }
                    }
                }

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Success",
                    Message = "Data downloaded successfully",
                    Cancel = "OK"
                });



            }
            catch (Exception)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageSync = string.Empty;
                SyncDownRetargetingText = "Download Data from Server";
                IsBusy = false;
                await Task.FromResult(0);
            }

        }

        #endregion Sync
    }
}