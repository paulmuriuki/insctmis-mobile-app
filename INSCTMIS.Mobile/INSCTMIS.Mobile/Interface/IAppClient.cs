﻿using System.Threading.Tasks;
using INSCTMIS.Mobile.Database;
using INSCTMIS.Mobile.Models;

namespace INSCTMIS.Mobile.Interface
{
    public interface IAppClient
    {
        Task<AccountResponse> ForgotPasswordAsync(string username);

        Task<AccountResponse> LoginAsync(string username, string password);

        Task<AccountResponse> LoginSocialWorker(string username, string password);
         
        Task<ListingOptionsResponse> GetListingSettings(AccountResponse accountResponse);

        Task<ApiStatus> ForgotPin(string nationalIdNo, string emailAddress, string id);

        Task<ApiStatus> ChangePin(string currentPin, string newPin, string id);

        Task<ApiStatus> UploadForm1AData(Form1A household, LocalDeviceInfo deviceInfo);
        Task<ApiStatus> UploadForm1BData(Form1B pregantLactatingWoman, LocalDeviceInfo deviceInfo);
        Task<ApiStatus> UploadForm1CData(Form1C caretaker, LocalDeviceInfo deviceInfo);

        Task LogoutAsync();

        Task<ComplianceResponse> GetHouseholdsReadyForCompliance(QueryParameter queryParameter);

        Task<MonitoringResponse> GetHouseholdsReadyForMonitoring(QueryParameter queryParameter);

        Task<RetargetingResponse> GetHouseholdsReadyForRetargeting(QueryParameter2 queryParameter);
    }
}