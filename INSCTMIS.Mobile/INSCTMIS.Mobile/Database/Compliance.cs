﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    public class ComplianceTDS
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public string WoredaName { get; set; }
        public Int32 KebeleID { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public double Age { get; set; }
        public string Sex { get; set; }
    }

    public class ComplianceTDSMembers
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public double Age { get; set; }
        public string Sex { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string NutritionalStatus { get; set; }
        public string EnrolledInSchool { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class ComplianceTDSPLW
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public Int32 KebeleID { get; set; }
        public Guid ProfileTDSPLWID { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public Int32 PLWAge { get; set; }
        public string BabySex { get; set; }
        public string PLW { get; set; }
        public string BabyName { get; set; }
        public DateTime? BabyDateOfBirth { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class ComplianceTDSCMC
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public Int32 KebeleID { get; set; }
        public Guid ProfileTDSCMCID { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string MalnourishmentDegree { get; set; }
        public DateTime ChildDateOfBirth { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class ComplianceCPHousehold 
    {
        [PrimaryKey, Unique]
        public Int32 UniqueID { get; set; }
        public Int32 RegionID { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 WoredaID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldMemberSex { get; set; }
        public Int32 HouseHoldMemberAge { get; set; }
        public string FiscalYear { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public string ClientType { get; set; }
    }

    public class ComplianceCPCase
    {
        [PrimaryKey, Unique]
        public Int32 UniqueID { get; set; }
        public Int32 RegionID { get; set; }
        public Int32 KebeleID { get; set; }
        public Int32 WoredaID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldMemberSex { get; set; }
        public Int32 HouseHoldMemberAge { get; set; }
        public string FiscalYear { get; set; }
        public Guid ProfileDSHeaderID { get; set; }
        public Guid ProfileDSDetailID { get; set; }
        public string ClientType { get; set; }
    }
}
