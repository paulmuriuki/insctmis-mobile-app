﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    public class RetargetingHousehold
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public string FormID { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfHouseholdHead { get; set; }
        public string HouseholdIDNumber { get; set; }
        public int FiscalYear { get; set; }
        public bool ApprovalStatus { get; set; }
    }

    public class RetargetingHouseholdDetail
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public string RetargetingDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public DateTime DateOfBirth { get; set; }
        public double Age { get; set; }
        public string Sex { get; set; }
        public int ClientCategory { get; set; }
        public string StartDateTDS { get; set; }
        public DateTime? NextCNStatusDate { get; set; }
        public string EndDateTDS { get; set; }
        public string DateTypeCertificate { get; set; }
        public string CaretakerID { get; set; }
        public string ChildID { get; set; }
    }
}
