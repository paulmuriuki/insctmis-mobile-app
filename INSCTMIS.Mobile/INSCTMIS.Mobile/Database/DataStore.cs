﻿using INSCTMIS.Mobile.Interface;
using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace INSCTMIS.Mobile.Database
{
    public class DataStore
    {
        private readonly SQLiteConnection _database;
        private readonly string nameSpace = "INSCTMIS.Mobile.Database.";

        public DataStore()
        {
            _database = DependencyService.Get<ISQLite>().GetConnection();

            this._database.CreateTable<SocialWorker>();
            this._database.CreateTable<Region>();
            this._database.CreateTable<Woreda>();
            this._database.CreateTable<Kebele>();
            this._database.CreateTable<IntegratedService>();
            this._database.CreateTable<ServiceProvider>();
            this._database.CreateTable<SystemCodeDetail>();

            _database.CreateTable<HouseholdMember>();
            _database.CreateTable<Form1A>();
            _database.CreateTable<Infant>();
            _database.CreateTable<Form1B>();
            _database.CreateTable<Form1C>();

            this._database.CreateTable<ComplianceTDS>();
            this._database.CreateTable<ComplianceTDSMembers>();
            this._database.CreateTable<ComplianceTDSPLW>();
            this._database.CreateTable<ComplianceTDSCMC>();
            this._database.CreateTable<ComplianceCPHousehold>();
            this._database.CreateTable<ComplianceCPCase>();

            this._database.CreateTable<MonitoringTDS1>();
            this._database.CreateTable<MonitoringTDS2>();
            this._database.CreateTable<MonitoringTDSPLW>();
            this._database.CreateTable<MonitoringTDSCMC>();
            this._database.CreateTable<MonitoringCPHousehold>();
            this._database.CreateTable<MonitoringCPCase>();

            this._database.CreateTable<RetargetingHousehold>();
            this._database.CreateTable<RetargetingHouseholdDetail>();

        }

        public virtual void AddOrUpdate<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.InsertOrReplace(entity);
        }

        public virtual void Create<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Insert(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Delete(entity);
        }

        public virtual void Delete<TEntity>(int primarykey) where TEntity : class
        {
            this._database.Delete<TEntity>(primarykey);
        }

        public virtual void Manage<TEntity>(TEntity entity) where TEntity : class
        {
        }

        public virtual void Update<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Update(entity);
        }

        #region SystemCodeDetail

        public List<SystemCodeDetail> SystemCodeDetailGetAll()
        {
            return this._database.Query<SystemCodeDetail>(
                "SELECT * FROM [SystemCodeDetail]");
        }

        public SystemCodeDetail SystemCodeDetailGetByCode(string childCode)
        {

            var sql2 =
                $"SELECT * FROM [SystemCodeDetail] WHERE [SystemCode] = '{childCode}' ";
            Debug.WriteLine(sql2);
            return this._database.Query<SystemCodeDetail>(sql2).Single();
        }

        public SystemCodeDetail SystemCodeDetailGetById(int id)
        {
            return this._database.Query<SystemCodeDetail>($"SELECT * FROM [SystemCodeDetail] WHERE [Id] = {id}").Single();
        }

        public List<SystemCodeDetail> SystemCodeDetailsGetByCode(string code)
        {
            //var systemCode = this._database.Query<SystemCodeDetail>($"select * from [SystemCodeDetail] where code = '{code}'").Single();

            var systemcodetails = this._database.Query<SystemCodeDetail>(
                $"SELECT * FROM [SystemCodeDetail] WHERE [SystemCode] ='{code}' ");
            return systemcodetails;
        }

        #endregion SystemCodeDetail

        #region Abstract Get

        public object GetCvRegData(int id)
        {
            //var query =
            //    $"SELECT * FROM [CvRegistration] R INNER JOIN [SubLocation] A ON R.SubLocationId = A.Id and R.Id = {id}";
            var query =
                $"SELECT * FROM [CvRegistration]";
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + "CvRegistration"));
            return _database.Query(map, query, obj).Single();
        }

        public List<object> GetTable(string tableName)
        {
            // tableName =  + tableName;
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM [" + tableName + "]";
            return _database.Query(map, query, obj).ToList();
        }

        public T GetTableRow<T>(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Cast<T>().Single();
        }

        public object GetTableRow(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Single();
        }

        public object GetTableRow(string tableName, string column1, string value1, string column2, string value2)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column1 + " = '" + value1 + "' AND " + column2 + " = '" + value2 + "' LIMIT 1";
            return _database.Query(map, query, obj).Single();
        }

        public List<object> GetTableRows(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public List<object> GetTableRows(string tableName, string column1, string value1, string column2, string value2)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column1 + " = '" + value1 + "' AND " + column2 + " = '" + value2 + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public List<T> GetTableRows<T>(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<T> GetTableRows<T>(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName;
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        #endregion Abstract Get
    }
}