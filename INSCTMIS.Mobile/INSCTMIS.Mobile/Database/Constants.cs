﻿namespace INSCTMIS.Mobile.Database
{
    public class Constants
    {
        public static string BaseApiAddress => "http://10.0.2.2/ethiopia_api/";
        public static string BaseSiteAddress => "http://10.0.2.2/ethiopia_api/";

        //Before uncommenting the lines below, remove (android:usesCleartextTraffic="true") from android manifest)

        /*
         public static string BaseApiAddress => "https://7a859048.ngrok.io/";
         public static string BaseSiteAddress => "https://7a859048.ngrok.io/";
         */

    }
}