﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    public class SocialWorker
    {
        [PrimaryKey, Unique]
        public Int64 UserId { get; set; }

        public Int32 RoleId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}