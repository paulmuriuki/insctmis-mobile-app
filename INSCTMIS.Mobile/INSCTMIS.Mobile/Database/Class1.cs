﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MCHMIS.Mobile.Database
{
    public class HouseholdReg
    {
        public string Id { get; set; }

        public string ParentId { get; set; }

        public int? WardId { get; set; }

        public int? SubLocationId { get; set; }

        public int? CountyId { get; set; }


        [DisplayName("PHYSICAL ADDRESS")]
        public string PhysicalAddress { get; set; }

        [DisplayName("NEAREST CHURCH/MOSQUE")]
        public string NearestReligiousBuilding { get; set; }

        [DisplayName("NEAREST SCHOOL")]
        public string NearestSchool { get; set; }

        [DisplayName("Duration of residence in this place: Years")]
        public int? ResidenceDurationYears { get; set; }
        [DisplayName("Months")]
        public int? ResidenceDurationMonths { get; set; }

        public string Village { get; set; }

        [DisplayName("Status")]
        public int StatusId { get; set; }

        public string DwellingStartDate { get; set; }

        public string CaptureStartDate { get; set; }

        public string CaptureEndDate { get; set; }

        public float? Longitude { get; set; }

        public float? Latitude { get; set; }

        public float? Elevation { get; set; }
        public DateTime? SyncDate { get; set; }


        [DisplayName("Phone Number")]
        public string Phone { get; set; }

        public string Institution { get; set; }

        public string MotherId { get; set; }


        

        [Ignore]
        public Ward Ward { get; set; }
        [Ignore]    
        public SubLocation SubLocation { get; set; }

        public decimal? PMTScore { get; set; }

        public int? TypeId { get; set; }

         
        [DisplayName("Next of Kin First Name (As recorded in the Health Handbook)")]
        public string NOKFirstName { get; set; }

        [DisplayName("Next of Kin Middle Name")]
        public string NOKMiddleName { get; set; }

        [DisplayName("Next of Kin Surname")]
        public string NOKSurname { get; set; }

        [DisplayName("Next of Kin Phone number")]
        public string NOKPhone { get; set; }

        [DisplayName("Next of Kin National ID number")]
        public string NOKIdNumber { get; set; }


        public int? InterviewStatusId { get; set; }
        public int? InterviewResultId { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail InterviewStatus { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail InterviewResult { get; set; }
    
        public string HouseholdId { get; set; }

        [DisplayName("No. of Habitable Rooms")]
        public int? HabitableRoomsNo { get; set; }

        public int IsOwnHouseId { get; set; }

        [DisplayName("Tenure Status")]
        public int TenureStatusId { get; set; }

        public string TenureStatusOther { get; set; }

        [DisplayName("Roof")]
        public int RoofMaterialId { get; set; }

        public string RoofMaterialOther { get; set; }

        [DisplayName("Wall")]
        public int WallMaterialId { get; set; }

        public string WallMaterialOther { get; set; }

        [DisplayName("Floor")]
        public int FloorMaterialId { get; set; }

        public string FloorMaterialOther { get; set; }

        [DisplayName("Dwelling Unit Risk")]
        public int UnitRiskId { get; set; }

        public string UnitRiskOther { get; set; }

        [DisplayName("Main source of WATER")]
        public int WaterSourceId { get; set; }

        public string WaterSourceOther { get; set; }

        [DisplayName("Main mode of HUMAN WASTE DISPOSAL")]
        public int ToiletTypeId { get; set; }

        public string ToiletTypeOther { get; set; }

        [DisplayName("Main type of COOKING FUEL")]
        public int CookingFuelSourceId { get; set; }

        public string CookingFuelSourceOther { get; set; }

        [DisplayName("Main type of LIGHTING FUEL")]
        public int LightingSourceId { get; set; }

        public string LightingSourceOther { get; set; }

        [DisplayName("LIVE BIRTHS in this household in the last 12 months")]
        public int LiveBirths { get; set; }

        [DisplayName("DEATHS in this household in the last 12 months")]
        public int Deaths { get; set; }

        [DisplayName("Conditions of your household")]
        public int HouseholdConditionId { get; set; }

        [DisplayName("In the past 7 days, did anyone in this household cut the size of the meals or skip meals because of the lack of enough money?")]
        public int HasSkippedMealId { get; set; }

        public SystemCodeDetail HasSkippedMeal { get; set; }

        [DisplayName("Benefits from Social Assistance Programmes")]
        public bool IsRecievingNSNPBenefit { get; set; }

        [DisplayName("Is anyone in this household receiving benefits from any other Social Assistance Programme or any other external support?")]
        public int IsReceivingOtherBenefitId { get; set; }

        public SystemCodeDetail IsReceivingOtherBenefit { get; set; }

        [DisplayName("Name of the PROGRAMME(s)")]
        public string OtherProgrammes { get; set; }

        [DisplayName("Type of BENEFIT received")]
        public int? OtherProgrammesBenefitTypeId { get; set; }

        [DisplayName("Amount of BENEFIT in the last receipt")]
        public decimal? OtherProgrammesBenefitAmount { get; set; }

        [DisplayName("IN-KIND of benefit")]
        public string OtherProgrammesInKindBenefit { get; set; }

        
        [DisplayName("Receives other kind of support from relatives or friends?")]
        public int? BenefitFromFriendsRelativeId { get; set; }

        public SystemCodeDetail BenefitFromFriendsRelative { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail TenureStatus { get; set; }
        [Ignore, JsonIgnore]
        public SystemCodeDetail RoofMaterial { get; set; }
        [Ignore, JsonIgnore]
        public SystemCodeDetail WallMaterial { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail FloorMaterial { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail UnitRisk { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail WaterSource { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail ToiletType { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail CookingFuelSource { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail LightingSource { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail HouseholdCondition { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail OtherProgrammesBenefitType { get; set; }



        public int ExoticCattle { get; set; }
        public int IndigenousCattle { get; set; }
        public int Sheep { get; set; }
        public int Goats { get; set; }
        public int Camels { get; set; }
        public int Donkeys { get; set; }
        public int Pigs { get; set; }
        public int Chicken { get; set; }



        public int IsTelevisionId { get; set; }

        public int IsMotorcycleId { get; set; }
        public int IsTukTukId { get; set; }
        public int IsRefrigeratorId { get; set; }
        public int IsCarId { get; set; }
        public int IsMobilePhoneId { get; set; }
        public int IsBicycleId { get; set; }




        public string DownloadDate { get; set; }
        [Ignore]
        public ICollection<RegistrationMember> RegistrationMembers { get; set; }


        [Ignore]
        public ICollection<RegistrationMemberDisability> RegistrationMemberDisabilities { get; set; }

        [Ignore]
        public ICollection<RegistrationProgramme> RegistrationProgrammes { get; set; }


        public string RegDate1 { get; set; }
        public string RegDate2 { get; set; }
        public string RegDate3 { get; set; }

        private const string delimiter = " ";
        private string haystack;
        [Newtonsoft.Json.JsonIgnore]
        public string Haystack
        {
            get
            {
                if (haystack != null)
                    return haystack;
                var builder = new StringBuilder();
                builder.Append(delimiter);
                builder.Append(Id);
                builder.Append(delimiter);
                builder.Append(Village);
                builder.Append(delimiter);
                builder.Append(PhysicalAddress);
                builder.Append(delimiter);
                builder.Append(NearestReligiousBuilding);
                builder.Append(delimiter);
                builder.Append(NearestSchool);
                haystack = builder.ToString();
                return haystack;
            }
        }


        [Ignore, JsonIgnore]
        public SystemCodeDetail IsTelevision { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsMotorcycle { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsTukTuk { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsRefrigerator { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsCar { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsMobilePhone { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsBicycle { get; set; }
        [Ignore, JsonIgnore]
        public SystemCodeDetail IsSkippedMeal { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsReceivingSocial { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail InKindBenefit { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail WasteDisposalMode { get; set; }
         
        [Ignore, JsonIgnore]
        public SystemCodeDetail WallConstructionMaterial { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsOwned { get; set; }

        

        [Ignore, JsonIgnore]
        public SystemCodeDetail RoofConstructionMaterial { get; set; }


        [Ignore, JsonIgnore]
        public SystemCodeDetail NsnpProgrammes { get; set; }

         

        [Ignore, JsonIgnore]
        public SystemCodeDetail LightingFuelType { get; set; }



        [Ignore, JsonIgnore]
        public SystemCodeDetail HouseHoldCondition { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail FloorConstructionMaterial { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail DwellingUnitRisk { get; set; }
         
        [Ignore, JsonIgnore]
        public SystemCodeDetail CookingFuelType { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail BenefitType { get; set; }
         
    }



}