﻿using System;
using SQLite;

namespace INSCTMIS.Mobile.Database
{
    public class MonitoringTDS1
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string SchoolName { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string Remarks { get; set; }
        public string ReportingPeriod { get; set; }
    }

    public class MonitoringTDS2
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldVisit { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public int Members { get; set; }
    }

    public class MonitoringTDSPLW
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfPLW { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public DateTime? BabyDateOfBirth { get; set; }
    }

    public class MonitoringTDSCMC
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string ReportingPeriod { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string MalnourishedChildSex { get; set; }
        public DateTime ChildDateOfBirth { get; set; }
    }

    public class MonitoringCPHousehold
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string ChildProtectionHeaderId { get; set; }
        public string ProfileDSHeaderId { get; set; }
        public string ProfileDSDetailId { get; set; }
        public string RegionName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string WoredaName { get; set; }
        public string GoteGare { get; set; }
        public string ClientType { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string FiscalYear { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime? ClosedOn { get; set; }
        public string ClosedBy { get; set; }
        public string ClosedReason { get; set; }
        public string CaseStatus { get; set; }
    }

    public class MonitoringCPCase
    {
        [PrimaryKey, Unique]
        public Int64 ColumnID { get; set; }
        public string ChildProtectionDetailId { get; set; }
        public string ChildProtectionHeaderId { get; set; }
        public int RiskId { get; set; }
        public int ServiceId { get; set; }
        public int ProviderId { get; set; }
    }

    
}
