﻿using INSCTMIS.Mobile.Database;
using Plugin.DeviceInfo.Abstractions;
using System.Collections.Generic;

namespace INSCTMIS.Mobile.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }

    public class ApiStatus
    {
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public int? Id { get; set; }
    }

    public class LocalDeviceInfo
    {
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public Platform Platform { get; set; }
        public Idiom Idiom { get; set; }
        public bool IsDevice { get; set; }
    }

    public class AuthKeyResponse
    {
        public int Success { get; set; }
    }

    public class AccountResponse
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Message { get; set; }
        public string IsAuthenticated { get; set; }
    }

    public class ListingOptionsResponse : SocialWorkerLoginResponse
    {
        public SocialWorker SocialWorker { get; set; }
        public List<Region> Region { get; set; }
        public List<Woreda> Woreda { get; set; }
        public List<Kebele> Kebele { get; set; }
        public List<ServiceProvider> ServiceProvider { get; set; }
        public List<IntegratedService> IntegratedService { get; set; }
        public List<SystemCodeDetail> SystemCodeDetail { get; set; }
    }

    public class SocialWorkerLoginResponse
    {
        public string Error { get; set; }
    }

    public class ComplianceResponse
    {
        public List<ComplianceTDS> TDSHouseholds { get; set; }
        public List<ComplianceTDSMembers> TDSHouseholdMembers { get; set; }

        public List<ComplianceTDSPLW> TDSPLWHouseholdDetails { get; set; }
        public List<ComplianceTDSCMC> TDSCMCHouseholdDetails { get; set; }
        public List<ComplianceCPHousehold> CPHouseholdDetails { get; set; }
        public List<ComplianceCPCase> CPCaseDetails { get; set; }
    }

    public class MonitoringResponse
    {
        public List<MonitoringTDS1> TDS1HouseholdDetails { get; set; }
        public List<MonitoringTDS2> TDS2HouseholdDetails { get; set; }
        public List<MonitoringTDSPLW> PLWHouseholdDetails { get; set; }
        public List<MonitoringTDSCMC> CMCHouseholdDetails { get; set; }
        public List<MonitoringCPHousehold> CPHouseholdDetails { get; set; }
        public List<MonitoringCPCase> CPCaseDetails { get; set; }
    }

    public class RetargetingResponse
    {
        public List<RetargetingHousehold> RetargetingHousehold { get; set; }
        public List<RetargetingHouseholdDetail> RetargetingHouseholdDetails { get; set; }
    }

    public class SelectableItemWrapper<T>
    {
        public bool IsSelected { get; set; }
        public T Item { get; set; }
    }

    public class QueryParameter
    {
        public string KebeleID { get; set; }
        public string ReportingPeriodID { get; set; }
    }

    public class QueryParameter2
    {
        public string KebeleID { get; set; }
        public string FiscalYear { get; set; }
    }
}